<?php

require_once __DIR__ . '/../Database/Db.php';

abstract class Model extends Db
{
    protected $table;
    protected $primaryKey;
    protected $columns;

    public function __construct(string $table, string $pk, $columns) 
    {
        $this->table = $table;
        $this->primaryKey = $pk;
        $this->columns = $columns;
    }

    protected function columnNames()
    {
        return implode(',',
            array_merge(
                [$this->primaryKey],
                $this->columns
            )
        );
    }

    public function createOrUpdate(array $values)
    {
        $connect = $this->connect();
        $keys = array_keys($values);
        $query = "INSERT INTO {$this->table} (" . implode(',', $keys) . ") VALUES(:" . implode(',:', $keys) . ")";
        try {
            $insert = $connect->prepare($query);
            $insert->execute($values);
         } catch (PDOException $e) {
            throw new Exception($e->getMessage(), 1);
        }
        return $connect->lastInsertID();
    }

    public function select(string $query, $all = false)
    {
        $connect = $this->connect();
        $result = $connect->prepare($query);
        $result->execute();
        if ($all) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return $result->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function get(string $columnOrder = null, $directionOrder = 'ASC')
    {
        $columnOrder = $columnOrder ?? $this->primaryKey;

        return $this->select("SELECT {$this->columnNames()} FROM {$this->table} ORDER BY {$columnOrder} {$directionOrder}", true);
    }

    public function getById($id)
    {
        return $this->select("SELECT {$this->columnNames()} FROM {$this->table} WHERE {$this->primaryKey}={$id}");
    }

    public function toArray($data)
    {
        if (is_array($data)) {
            
        } else {
            return $data;
        }
    }
}