<?php

require_once 'Model.php';
require_once 'DocumentType.php';

class Person extends Model
{
    function __construct()
    {
        parent::__construct(
            'persons',
            'id',
            [
                'document',
                'first_name',
                'last_name',
                'document_type_id'
            ]
        );
    }

    public function toArray($data)
    {
        if (is_array($data)) {
            $result = [];
            foreach ($data as $row) {
                $row['document_type'] = $this->typeDocument($row['document_type_id']);
                unset($row['document_type_id']);
                $result[] = $row;
            }
            return $result;
        } else {
            $data['document_type'] = $this->typeDocument($data['document_type_id']);
            unset($data['document_type_id']);
            return $data;
        }
    }

    public function typeDocument($document_type_id)
    {
        $model = new DocumentType();
        return $model->getById($document_type_id);
    }
}
