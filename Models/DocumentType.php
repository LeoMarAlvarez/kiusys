<?php 
require_once 'Model.php';

class DocumentType extends Model
{
    function __construct() 
    {
        parent::__construct(
            'document_types',
            'id',
            [
                'name'
            ]
        );
    }
}