# KIUSYS Challenge

### Pasos para correr Proyecto

1.  clonar repo
    * git clone https://gitlab.com/LeoMarAlvarez/kiusys.git
2.  configurar las variables de conexión a la db
    * Editar archivo Database/config.php
3.  levantar proyecto
    * php -S localhost:8000
