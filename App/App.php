<?php

require_once 'helpers.php';

class App
{
    public function __construct(Array $path)
    {
        switch($path[1]) {
            case 'persons':
                require_once __DIR__ . '/../Controllers/PersonController.php';
                new PersonController();
            default:
                error(
                    'Page Not Found ',
                    'Error',
                    404
                );
        }
    }
}