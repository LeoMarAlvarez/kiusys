<?php

if (!function_exists('response')) {
    function response(string $message, array $data = [], string $title = 'Successfull!', int $status = 200)
    {
        http_response_code($status);
        echo json_encode([
            'title' => $title,
            'message' => $message,
            'data' => $data
        ]);
        exit;
    }
}

if (!function_exists('error')) {
    function error(string $error, string $title = 'Error', $status = 419)
    {
        http_response_code($status);
        echo json_encode([
            'title' => $title,
            'error' => $error
        ]);
        exit;
    }
}

if (!function_exists('unauthorized'))
{
    function unauthorized()
    {   
        header("HTTP/1.1 401 Unauthorized");
        error("you don't have permissions", 'Unauthorized', 401);
        exit;
    }
}