<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

header("Content-Type: application/json; charset=UTF-8");
header("Accept: application/json; charset=UTF-8");

require_once 'App/App.php';

$url = explode('/', $_SERVER['PATH_INFO']);

new App($url);