<?php

abstract class Controller
{
    protected $methodsAvailable;

    function __construct()
    {
        if (!in_array($_SERVER['REQUEST_METHOD'], $this->methodsAvailable)) {
            error('Method Not Allowed', 'Error', 405);
        }
    }
}