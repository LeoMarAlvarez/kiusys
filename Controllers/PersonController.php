<?php

require_once 'Controller.php';
require_once __DIR__ . '/../Models/Person.php';

class PersonController extends Controller
{
    protected $model;
    function __construct()
    {
        $this->methodsAvailable = ['GET', 'POST'];
        parent::__construct();
        $this->model = new Person();
        switch ($_SERVER['REQUEST_METHOD']){
            case 'GET':
                $this->index();
            case 'POST':
                $this->store();
        }
    }

    public function index()
    {
        try {
            $request = $_REQUEST;
            if (isset($request['id'])) {
                $data = $this->model->getById($request['id']);
            } else {
                $data = $this->model->get($request['order'] ?? null, $request['direction'] ?? null);
            }
            
            response(
                'List of Person',
                $this->model->toArray($data)
            );
        } catch (Exception $e) {
            error($e->getMessage());
        }
    }

    public function store()
    {
        $postBody = (array) json_decode(file_get_contents("php://input"));
        $requiredColumns = ['first_name', 'last_name', 'document', 'document_type_id'];
        foreach ($requiredColumns as $column) {
            if (!in_array($column, array_keys($postBody))) {
                error(
                    "The column {$column} is required",
                    'Column required'
                );
            }
        }

        try {
            $response = $this->model->createOrUpdate($postBody);
            response(
                'The person was created',
                $this->model->toArray($this->model->getById($response))
            );
        } catch (Exception $e) {
            error($e->getMessage());
        }
    }
}
