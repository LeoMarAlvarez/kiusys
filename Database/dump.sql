DROP DATABASE IF EXISTS kiusys;

CREATE DATABASE kiusys;

USE kiusys;

CREATE TABLE document_types (
	`id` mediumint AUTO_INCREMENT,
	`name` varchar(100) unique not null,
	`deleted_at` datetime default null,
	primary key (`id`)
);

CREATE TABLE persons (
	`id` int AUTO_INCREMENT,
	`first_name` varchar(200) not null,
	`last_name` varchar(200) not null,
	`document_type_id` mediumint not null,
	`document` varchar(30) unique not null,
	primary key (`id`),
	foreign key (`document_type_id`) references `document_types`(`id`)
);