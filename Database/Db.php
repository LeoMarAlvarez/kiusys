<?php
require_once 'config.php';

abstract class Db {

    public function connect()
    {
        try{
            $conexion = 'mysql:host='.HOST.';port='.PORT.';dbname='.DBNAME.';charset=utf8';
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            $pdo = new PDO($conexion, USER , PWD , $options);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;

        }catch(PDOException $e){
            throw new Exception('Error en Conexion : '. $e->getMessage());
        }
    }
}